# --*-- coding:utf-8 --*--

"""
==input==   整数数组
==******==
==output==  归并排序后的整数数组
##########  在O（N）的空间复杂度和O（N*logN）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


# 合并左边和右边两个数组
def merge(left, right):
    res = []
    i = j = 0
    while i < len(left) and j < len(right):
        if left[i] < right[j]:
            res.append(left[i])
            i += 1
        else:
            res.append(right[j])
            j += 1
    if i == len(left):
        res.extend(right[j:])
    if j == len(right):
        res.extend(left[i:])
    return res


def merge_sort(array):
    if len(array) < 2:
        return array
    middle = len(array) // 2
    left = merge_sort(array[:middle])   # 拆分左边递归排序
    right = merge_sort(array[middle:])  # 拆分右边递归排序
    return merge(left, right)


if __name__ == '__main__':
    a_list = [1, 8, 2, 1, 0, 9, 6, 4, 0, 0, 3]
    assert merge_sort(a_list) == [0, 0, 0, 1, 1, 2, 3, 4, 6, 8, 9]
