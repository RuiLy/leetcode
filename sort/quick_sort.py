# --*-- coding:utf-8 --*--
"""
==input==   整数数组
==******==
==output==  快速排序后的整数数组
##########  在O（N）的空间复杂度和O（N*logN）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def quick_sort(array, start, end):
    if start >= end:
        return
    low = start
    high = end
    pivot = array[low]  # 找基准值 (初始基准值是数组最左边的值，所以下面要先从最右边开始找比基准值小的数)
    while low < high:
        while low < high and array[high] >= pivot:  # 从右边开始找比基准值小的数
            high -= 1
        array[low] = array[high]
        while low < high and array[low] < pivot:  # 从左边开始找比基准值大的数
            low += 1
        array[high] = array[low]
    array[low] = pivot
    quick_sort(array, start, low - 1)  # 递归排左边的数组
    quick_sort(array, low + 1, end)  # 递归排右边的数组


if __name__ == '__main__':
    a_list = [1, 8, 2, 1, 0, 9, 6, 4, 0, 0, 3]
    quick_sort(a_list, 0, len(a_list) - 1)
    assert a_list == [0, 0, 0, 1, 1, 2, 3, 4, 6, 8, 9]
