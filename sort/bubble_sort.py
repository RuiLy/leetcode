# --*-- coding:utf-8 --*--

"""
==input==   整数数组
==******==
==output==  冒泡排序后的整数数组
##########  在O（1）的空间复杂度和O（N*N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def bubble_sort(arr_list):

    length = len(arr_list)
    if length < 2:
        return arr_list
    for i in range(0, length):
        for j in range(0, length - i - 1):
            if arr_list[j] > arr_list[j + 1]:
                arr_list[j], arr_list[j + 1] = arr_list[j + 1], arr_list[j]
    return arr_list


if __name__ == '__main__':
    arr = [0, 5, 5, 9, 2, 3, 1, 2]
    assert bubble_sort(arr) == [0, 1, 2, 2, 3, 5, 5, 9]

