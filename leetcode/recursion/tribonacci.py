# -*- coding:utf-8 -*-
"""
==input==   泰波那契第 n 位
==******==  第0为0   第1位1  第2位1  第3位 0+1+1 = 2  第4位 1+1+2=4
==output==  泰波那契第 n 位的值
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 非递归和尾递归的理解
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def tribonacci(nums):
    return method3(nums)


# 递归
def method1(nums):
    if nums == 0 or nums == 1:
        return nums
    if nums == 2:
        return 1
    return method1(nums - 3) + method1(nums - 2) + method1(nums - 1)


# 尾递归
def method2(nums, acc1, acc2, acc3):
    if nums == 0:
        return acc1
    if nums == 1:
        return acc2
    if nums == 2:
        return acc3
    return method2(nums - 1, acc2, acc3, acc1 + acc2 + acc3)


# 非递归实现
def method3(nums):
    if nums == 0 or nums == 1:
        return nums
    if nums == 2:
        return 1
    acc1, acc2, acc3, cur = 0, 1, 1, 3
    while cur <= nums:
        acc1, acc2, acc3 = acc2, acc3, acc1 + acc2 + acc3
        cur = cur + 1
    return acc3


if __name__ == '__main__':
    n = 25
    assert tribonacci(n) == 1389537
