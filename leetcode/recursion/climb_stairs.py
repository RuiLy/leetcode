# -*- coding:utf-8 -*-
"""
==input==   楼梯数
==******==  每次只能走一步或者两步楼梯，计算总共有多少种走法
==output==  一共有多少种走法
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 尾递归和非递归的实现方式
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def climb_stairs(nums):
    return method3(nums)


# 递归
def method1(nums):
    if nums == 1 or nums == 2:
        return nums
    return method1(nums - 1) + method1(nums - 2)


# 尾递归
def method2(nums, acc1, acc2):
    if nums == 1:
        return acc1
    if nums == 2:
        return acc2
    return method2(nums - 1, acc2, acc1 + acc2)


# 非递归
def method3(nums):
    if nums == 1 or nums == 2:
        return nums
    acc1, acc2, cur = 1, 2, 3
    while cur <= nums:
        temp = acc2
        acc2 = acc2 + acc1
        acc1 = temp
        cur = cur + 1
    return acc2


if __name__ == '__main__':
    storey = 9
    assert climb_stairs(storey) == 55
