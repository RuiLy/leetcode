# -*- coding:utf-8 -*-
"""
==input==   楼梯数
==******==  每次最多走 1 至 m 步楼梯，计算总共有多少种走法
==output==  一共有多少种走法
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def climb_stairs_m(nums, max_m):
    return method1(nums, max_m)


def method1(nums, max_m):
    res = 0
    if nums == 0:
        return 1
    if nums >= max_m:
        for i in range(max_m):
            res += method1(nums - (i + 1), max_m)
    else:
        res = method1(nums, nums)
    return res


if __name__ == '__main__':
    n = 4
    m = 3
    assert climb_stairs_m(n, m) == 7
