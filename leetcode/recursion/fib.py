# -*- coding:utf-8 -*-
"""
==input==   斐波那契第 n 位
==******==
==output==  斐波那契第 n 位的值
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 非递归和尾递归的理解
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def fib(nums):
    return method3(nums)


# 递归
def method1(nums):
    if nums == 0 or nums == 1:
        return nums
    return method1(nums - 1) + method1(nums - 2)


# 尾递归
def method2(nums, acc1, acc2):
    if nums == 0:
        return acc1
    if nums == 1:
        return acc2
    return method2(nums - 1, acc2, acc1 + acc2)


# 非递归的迭代方式
def method3(nums):
    if nums == 0 or nums == 1:
        return nums
    acc1, acc2, cur = 0, 1, 2
    while cur <= nums:
        temp = acc1
        acc1 = acc2
        acc2 = temp + acc2
        cur = cur + 1
    return acc2


if __name__ == '__main__':
    n = 6
    assert fib(n) == 8
