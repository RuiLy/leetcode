# -*- coding:utf8 -*-
"""
==input==   一个有序的整数数组 和 一个目标值
==******==  从数组中查找该目标值，如果存在返回目标值在数组中的位置；如果不存在，返回目标值应该插入到数组的位置，使之保持有序特性
==output==  返回目标值应该在数组中的下标位置
##########  在O（1）的空间复杂度和低于O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 二分算法  ========>> 要求被查找的数组有序
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def search_insert_get_index(arrays, target):
    if arrays is None or len(arrays) == 0:
        return
    return method1(arrays, target)


def method1(arrays, target):
    low = 0
    high = len(arrays)
    while low < high:
        middle = low + (high - low) // 2
        if arrays[middle] < target:
            low = middle + 1
        elif arrays[middle] > target:
            high = middle
        else:
            return middle
    return high


def method2(arrays, target):
    for i in range(len(arrays)):
        if arrays[i] >= target:
            return i
    return len(arrays)


if __name__ == '__main__':
    input_list = [1, 3, 4, 5, 6, 9, 11, 12]
    num = 11
    out_index = search_insert_get_index(input_list, num)
    assert out_index == 6
