# -*- coding:utf8 -*-
"""
==input==   两个有序数组num1，num2和各自数组的长度（0在较长数组num1中起占位符的作用）
==******==  把num2数组的元素插入到num1中，要求num1仍然有序
==output==  有序的num1
##########  在O（1）的空间复杂度和不高于O（M + N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 M + N的时间复杂度，原地排序
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def merge_two_sort_array(nums1, m, nums2, n):
    if n == 0:
        return nums1
    return method1(nums1, m, nums2, n)


def method1(nums1, m, nums2, n):
    left = m - 1
    right = n - 1
    k = m + n - 1
    while left >= 0 and right >= 0:
        if nums2[right] >= nums1[left]:
            nums1[k] = nums2[right]
            k = k - 1
            right = right - 1
        else:
            nums1[k] = nums1[left]
            left = left - 1
            k = k - 1
    while right >= 0:
        nums1[k] = nums2[right]
        k = k - 1
        right = right - 1
    return nums1


def method2(nums1, m, nums2, n):
    for index in range(len(nums1) - m):
        nums1.pop(m)
    left, right = 0, 0
    while left < len(nums1) and right < n:
        if nums1[left] <= nums2[right]:
            left = left + 1
        else:
            nums1.insert(left, nums2[right])
            left = left + 1
            right = right + 1
    while right < n:
        nums1.insert(left, nums2[right])
        left = left + 1
        right = right + 1
    return nums1


if __name__ == '__main__':
    nums1 = [-1, 0, 1, 0, 0]
    m = 3
    nums2 = [1, 3]
    n = 2
    assert merge_two_sort_array(nums1, m, nums2, n) == [-1, 0, 1, 1, 3]
