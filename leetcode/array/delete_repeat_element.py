# -*- coding:utf-8 -*-
"""
==input==   有序的整数数组
==******==
==output==  删除重复元素后的数组长度
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 原地删除重复元素 O(1)的空间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def delete_repeat_element(nums):
    if not nums:
        return 0
    n = len(nums)
    slow = fast = 1
    while fast < n:
        if nums[fast] != nums[fast - 1]:
            nums[slow] = nums[fast]
            slow += 1
        fast += 1
    return slow


if __name__ == '__main__':
    test_list = [1, 1, 2, 3, 3, 3, 5]
    assert delete_repeat_element(test_list) == 4
