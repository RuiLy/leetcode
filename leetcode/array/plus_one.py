# -*- coding:utf-8 -*-
"""
==input==   整数组成的数组
==******==
==output==  数组加1后的，数组结果
##########  在O（N）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 进位的理解
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def plus_one(nums):
    if nums is None or len(nums) == 0:
        return nums
    return method1(nums)


def method1(nums):
    max_length = len(nums)
    bit = 1
    while max_length > 0:
        tmp = nums[max_length - 1] + bit
        if tmp >= 10:
            nums[max_length - 1] = int(str(tmp)[1])
            bit = 1
        else:
            nums[max_length - 1] = tmp
            bit = 0
        max_length -= 1
    if bit == 1:
        nums.insert(0, bit)
    return nums


if __name__ == '__main__':
    num_array = [9, 9, 9, 9]
    assert plus_one(num_array) == [1, 0, 0, 0, 0]
