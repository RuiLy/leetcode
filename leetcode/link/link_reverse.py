# --*--coding:utf-8 --*--
"""
==input==   单向链表反转
==******==
==output==  反转之后的单向链表
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（N）的时间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


class Node:
    def __init__(self, val=None):
        self.val = val
        self.nex = None


class Solution:

    def __init__(self):
        self.head = Node()

    def add(self, value):
        new = Node(value)
        new.nex = self.head.nex
        self.head.nex = new

    def add_list(self, val_list):
        list(map(lambda x: self.add(x), val_list[::-1]))

    def reverse(self):
        return self.method1(self.head.nex)

    @staticmethod
    def iterator(head):
        if head.val:
            if head and head.nex:
                print(str(head.val) + "-> ", end="")
            elif head and not head.nex:
                print(str(head.val), end="")
            else:
                return
        while head.nex:
            if head.nex.nex:
                print(str(head.nex.val) + "-> ", end="")
            else:
                print(str(head.nex.val), end="")
            head = head.nex
        print("\n")

    @staticmethod
    def method1(head):
        if not head or not head.nex:
            return head
        new_head = solution.method1(head.nex)
        head.nex.nex = head
        head.nex = None
        return new_head


if __name__ == '__main__':
    solution = Solution()
    solution.add_list([1, 2, 3, 4, 5])
    solution.iterator(solution.head)
    head_new = solution.reverse()
    solution.iterator(head_new)









