# -*- coding:utf8 -*-
"""
==input==   两个有序链表
==******==  将两个有序链表有序地串起来
==output==  返回合并后的链表，要求有序
##########  在O（N + M）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 递归方式实现的理解
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


class ListNode(object):
    def __init__(self, element):
        self.value = element
        self.next = None


def merge_link_list(link1, link2):
    if link1 is None:
        return link2
    if link2 is None:
        return link1
    return method1(link1, link2)


def method1(link1, link2):
    if link1 is None:
        return link2
    if link2 is None:
        return link1
    if link1.value < link2.value:
        link1.next = method1(link1.next, link2)
        return link1
    else:
        link2.next = method1(link1, link2.next)
        return link2


def method2(link1, link2):
    prehead = ListNode(-1)
    head = prehead
    while link1 and link2:
        if link1.value < link2.value:
            prehead.next = link1
            prehead = prehead.next
            link1 = link1.next
        else:
            prehead.next = link2
            prehead = prehead.next
            link2 = link2.next
    prehead.next = link2 if link1 is None else link1
    return head.next


def show(head):
    while head is not None:
        print(str(head.value), end="")
        head = head.next
        if head is not None:
            print("->", end="")
    print()


if __name__ == '__main__':
    x_head = ListNode(-1)
    x = x_head
    x.next = ListNode(1)
    x = x.next
    x.next = ListNode(2)
    x = x.next
    x.next = ListNode(2)
    x = x.next
    x.next = ListNode(4)
    x = x.next
    x.next = ListNode(6)
    x = x.next
    x.next = ListNode(9)
    x = x.next

    y_head = ListNode(-1)
    y = y_head
    y.next = ListNode(0)
    y = y.next
    y.next = ListNode(1)
    y = y.next
    y.next = ListNode(5)
    y = y.next
    y.next = ListNode(7)
    y = y.next

    show(x_head.next)
    show(y_head.next)
    resList = merge_link_list(x_head.next, y_head.next)
    show(resList)
