# --*--coding:utf-8 --*--
"""
==input==   判断单向链表是否有环
==******==
==output==  True/False   有环或者无环
##########  在O（1）的空间复杂度和低于O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的空间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


class Node:
    def __init__(self, val=None):
        self.val = val
        self.nex = None


class Solution:
    def __init__(self):
        self.head = Node()

    def add(self, value):
        new = Node(value)
        new.nex = self.head.nex
        self.head.nex = new

    def iterator(self):
        while self.head.nex:
            if self.head.nex.nex:
                print(str(self.head.nex.val) + "-> ", end="")
            else:
                print(str(self.head.nex.val), end="")
            self.head = self.head.nex

    def build_cycle(self, pos):
        cur, tail = self.head, self.head
        while pos:
            pos -= 1
            cur = cur.nex
        while tail.nex:
            tail = tail.nex
        tail.nex = cur

    def has_cycle(self):
        return self.method2(self.head.nex)

    # hash表存储遍历过的节点的地址
    # 如果遍历的节点已经在hash表中，则存在环
    # 否则把当前节点放到hash表中
    @staticmethod
    def method1(cur):
        hash_sets = set()
        while cur:
            if cur in hash_sets:
                return True
            hash_sets.add(cur)
            cur = cur.nex
        return False

    # 快慢指针法
    # 慢指针初始指向头节点，快指针初始指向头节点的写一个节点
    # 如果链表只有头节点或者只有一个节点，不存在环
    # 如果慢指针与快指针相遇，则存在环
    # 否则慢指针走一步，快指针走两步，直到快指针走到了头
    @staticmethod
    def method2(cur):
        if not cur or not cur.nex:
            return False
        low, fast = cur, cur.nex
        while low != fast:
            if not fast or not fast.nex:
                return False
            low = low.nex
            fast = fast.nex.nex
        return True


if __name__ == '__main__':
    so = Solution()
    input_node, position = [3, 2, 0, -4], 3
    for node in input_node[::-1]:
        so.add(node)
    so.build_cycle(pos=position)
    print(so.has_cycle())
    #so.iterator()

