# -*- coding:utf-8 -*-
"""
==input==   输入一个有序链表
==******==  对链表中的重复值进行去重
==output==  返回去重后的链表，要求仍然有序
##########  在O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 理解链表节点如何串联
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


class ListNode:
    def __init__(self, x):
        self.value = x
        self.next = None


def delete_duplicates(head):
    if head is None or head.next is None:
        return head
    return method1(head)


# 非递归方式
def method1(head):
    tmp = head
    while tmp.next:
        if tmp.value == tmp.next.value:
            tmp.next = tmp.next.next
        else:
            tmp = tmp.next
    return head


# 递归方式
def method2(head):
    if head is None or head.next is None:
        return head
    head.next = method2(head.next)
    if head.value == head.next.value:
        return head.next
    return head


def link_to_list(head):
    nums = list()
    node = ListNode(-1)
    node.next = head
    head = node
    while head.next:
        nums.append(head.next.value)
        head.next = head.next.next
    return nums


def list_to_link(nums):
    head = ListNode(-1)
    for item in nums[::-1]:
        node = ListNode(item)
        node.next = head.next
        head.next = node
    return head


if __name__ == '__main__':
    list_nums = [1, 1, 2, 3, 3]
    link_head = list_to_link(list_nums)
    assert link_to_list(delete_duplicates(link_head.next)) == [1, 2, 3]
