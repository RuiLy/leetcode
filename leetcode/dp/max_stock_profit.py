# -*- coding:utf-8 -*-
"""
==input==   整数组成的序列
==******==
==output==  能够获得的最大收益
##########
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 动态规划算法实现
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def max_stock_profile(prices):
    if prices is None or len(prices) < 2:
        return 0
    return method2(prices)


def method1(prices):
    length = len(prices)
    db = [[0 for i in range(2)] for j in range(length)]
    db[0][0] = 0
    db[0][1] = -prices[0]
    for i in range(length):
        if i == 0:
            continue
        db[i][0] = max(db[i - 1][0], db[i - 1][1] + prices[i])
        db[i][1] = max(db[i - 1][1], db[i - 1][0] - prices[i])
    return db[length - 1][0]


def method2(prices):
    length = len(prices)
    hold = -prices[0]
    no_hold = 0
    for i in range(length):
        if i == 0:
            continue
        hold = max(hold, no_hold - prices[i])
        no_hold = max(no_hold, hold + prices[i])
    return no_hold


if __name__ == '__main__':
    test_list = [7, 1, 5, 3, 6, 4]
    assert max_stock_profile(test_list) == 7


