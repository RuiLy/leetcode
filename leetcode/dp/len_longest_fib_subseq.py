# -*- coding:utf-8 -*-
"""
==input==   整数组成的序列
==******==
==output==  满足斐波那契规律的最长子序列长度，没有这样的子序列返回0
##########
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 动态规划算法实现
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def len_longest_fib_subseq(inputs):
    if input_list is None or len(inputs) == 0:
        return 0
    return method2(inputs)


# 动态规划
def method1(inputs):
    global_max = 0
    matrix = [[2 for _ in range(len(inputs))] for _ in range(len(inputs))]

    for i in range(len(inputs)):
        for j in range(i + 1, len(inputs)):
            for k in range(i - 1, -1, -1):
                if inputs[i] + inputs[k] == inputs[j]:
                    matrix[i][j] = max(matrix[i][j], matrix[k][i] + 1)
            global_max = max(global_max, matrix[i][j])
    return global_max if global_max > 2 else 0


# 动态规划优化版
def method2(inputs):
    global_max = 0
    matrix = [[2 for _ in range(len(inputs))] for _ in range(len(inputs))]
    init_map = {value: index for index, value in enumerate(inputs)}

    for i in range(len(inputs)):
        for j in range(i + 1, len(inputs)):
            k = init_map.get(inputs[j] - inputs[i])
            if k is not None and k < i:
                matrix[i][j] = max(matrix[i][j], matrix[k][i] + 1)
                global_max = max(global_max, matrix[i][j])
    return global_max if global_max > 2 else 0


# 暴力解法
def method3(inputs):
    sets = set(inputs)
    global_max = 0
    for i in range(len(inputs)):
        for j in range(i+1, len(inputs)):
            tmp_max = 2
            pre, post = inputs[i], inputs[j]
            while pre + post in sets:
                pre, post = post, pre + post
                tmp_max += 1
            global_max = global_max if global_max > tmp_max else tmp_max
    return global_max if global_max > 2 else 0


if __name__ == '__main__':
    input_list = [1, 12, 13, 14, 25, 38, 40, 63]
    assert len_longest_fib_subseq(input_list) == 6
