# -*- coding:utf-8 -*-
"""
==input==   第n项
==******==  第1项 '1'  第2项 '11'  第3项'21'  第4项'1211'  第5项 '111221'
==output==  外观数列
##########  在O（1）的空间复杂度和O（N*N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 从第一项累加到第n项的逆序思维
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def count_and_say(num):
    return method1(num)


def method1(num):
    res = "1"
    if num == 1:
        return res
    while num > 1:
        res += "#"
        count = 1
        tmp = ""
        for index in range(len(res) - 1):
            if res[index] == res[index + 1]:
                count = count + 1
            else:
                tmp += str(count) + res[index]
                count = 1
        res = tmp
        num = num - 1
    return res


if __name__ == '__main__':
    n = 9
    assert count_and_say(n) == "31131211131221"
