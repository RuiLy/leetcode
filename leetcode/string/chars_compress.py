# --*-- coding:utf-8 --*--
"""
==input==   给定一个大小写字母构成的字符数组
==******==  eg: ['a','a','a','B','c','d','d']  -> ['a','3','B','c','2','d']
==******==  eg: ['A','h','d','k','l']  -> ['A','h','d','k','l']
==output==  压缩后的字符串
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O(1)的空间复杂度，基于原数组进行压缩
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def chars_compress(chars):
    if chars is None or len(chars) < 2:
        return chars
    return method1(chars)


def method1(chars):
    index, counter, length = 1, 1, len(chars)
    while index < length:
        if chars[index - 1] == chars[index]:
            counter += 1
            chars.pop(index)
            length -= 1
        else:
            if counter == 1:
                index += 1
            else:
                str_counter = str(counter)
                len_counter = len(str_counter)
                for ch in str_counter:
                    chars.insert(index, ch)
                    length += 1
                index += (len_counter + 1)
            counter = 1
    if counter > 1:
        str_counter = str(counter)
        for ch in str_counter:
            chars.insert(index, ch)
    return chars


if __name__ == '__main__':
    input_chars = ['a', 'a', 'B', 'B', 'B', 'C', 'd', 'd', 'e', 'e']
    assert chars_compress(input_chars) == ['a', '2', 'B', '3', 'C', 'd', '2', 'e', '2']

