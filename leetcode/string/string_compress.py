# --*-- coding:utf-8 --*--
"""
==input==   给定一个大小写字母构成的字符串
==******==  eg: aaaBccd  -> a3Bc2d     Ahdkl  -> Ahdkl
==output==  压缩后的字符串
##########  在O（N）的空间复杂度和低于O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 尽量减少时间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def string_compress(input_str):
    if input_str is None or len(input_str) < 2:
        return input_str
    return method2(input_str)


def method2(input_str):
    low, fast, counter, length, res = 0, 1, 1, len(input_str), list()
    while fast < length:
        if input_str[fast] == input_str[low]:
            counter += 1
        else:
            res.append(input_str[low])
            counter = fast - low
            if counter > 1:
                for k in str(counter):
                    res.append(k)
            low = fast
            counter = 1
        fast += 1
    res.append(input_str[low])
    if counter > 1:
        for k in str(counter):
            res.append(k)
    return input_str if len(res) > length else "".join(res)


if __name__ == '__main__':
    input_string = "aaBBBCddee"
    assert string_compress(input_string) == "a2B3Cd2e2"


