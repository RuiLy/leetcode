# --*-- coding:utf-8 --*--
"""
==input==   给定一个字符串
==******==  规定空串是回文串，只考虑字母和数字，且字母忽略大小写 eg: ""  -> True     "A man, a plan, a canal: Panama"  -> True
==output==  是否是回文字符串
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 首尾指针的运用
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def is_palindrome(input_s):
    if input_s is None or len(input_s) == 0:
        return True
    return method1(input_s)


def method1(input_s):
    first, last, flag = 0, len(input_s) - 1, True
    while first < last:
        first_ch, last_ch = input_s[first], input_s[last]
        if not first_ch.isalpha() and not first_ch.isdigit():
            first += 1
            continue
        if not last_ch.isalpha() and not last_ch.isdigit():
            last -= 1
            continue
        if first_ch.lower() != last_ch.lower():
            flag = False
            break
        first += 1
        last -= 1
    return flag


if __name__ == '__main__':
    input_str = "0P"
    assert is_palindrome(input_str) is False
