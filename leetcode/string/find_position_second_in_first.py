# -*- coding:utf8 -*-
"""
==input==   给定一个待匹配的字符串，和一个目标字符串
==******==  要求从待匹配的字符串中，找出目标字符串的位置
==output==  返回目标字符串在待匹配字符串中的第一个位置下标；若目标字符串为空 或者 目标字符串没在待匹配的字符串中，返回-1
##########  在O（1）的空间复杂度和低于O（N * M）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 kmp 算法
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def find_position_second_in_first(string_one, string_two):
    if string_two is None or len(string_two) == 0:
        return 0
    if string_one is None or len(string_one) == 0:
        return -1
    if len(string_two) > len(string_one):
        return -1
    return method2(string_one, string_two)


def method1(haystack, needle):
    for index in range(len(haystack)):
        if haystack[index: index + len(needle)] == needle:
            return index
    return -1


def method2(haystack, needle):
    index1, index2 = 0, 0
    while index1 < len(haystack) and index2 < len(needle):
        if haystack[index1] == needle[index2]:
            index1 = index1 + 1
            index2 = index2 + 1
        else:
            index1 = index1 - index2 + 1
            index2 = 0
    return index1 - len(needle) if index2 == len(needle) else -1


def kmp(haystack, needle):

    return -1


if __name__ == '__main__':
    first_string = "hello"
    second_string = "lle"
    out_int = find_position_second_in_first(first_string, second_string)
    assert out_int == -1





