# -*- coding:utf-8 -*-
"""
==input==   带压缩的字符数组
==******==
==output==  压缩后的长度
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O(1)的空间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def compress_string(chars):
    if chars is None or len(chars) == 0:
        return 0
    return method1(chars)


# 在原数组的基础上，直接得到压缩后的结果
def method1(chars):
    count = 1
    if len(chars) == 1:
        return count
    index = 0
    while index < len(chars):
        if index + 1 < len(chars) and chars[index] == chars[index + 1]:
            chars.pop(index + 1)
            count += 1
        else:
            if count == 1:
                index += 1
            else:
                count_str = str(count)
                for k in range(len(count_str)):
                    chars.insert(index + k + 1, count_str[k])
                    index += k
                index += 2
                count = 1
    return len(chars)


if __name__ == '__main__':
    input_str = ["a", "b", "b", "2", "2", "2", "c", "c"]
    assert compress_string(input_str) == 7


