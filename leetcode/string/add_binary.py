# -*- coding:utf-8 -*-
"""
==input==   两个二进制（只包含0或1）的字符串
==******==
==output==  两个字符串的和
##########
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 进位的处理
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def add_binary(str1, str2):
    if str1 is None or len(str1) == 0:
        return str2
    if str2 is None or len(str2) == 0:
        return str1
    return method2(str1, str2)


def method2(str1, str2):
    length_str1, length_str2 = len(str1), len(str2)
    max_length = length_str1 if length_str1 > length_str2 else length_str2
    res_list = ""
    for index in range(max_length - length_str1):
        str1 = '0' + str1
    for index in range(max_length - length_str2):
        str2 = '0' + str2
    bit = 0
    while max_length > 0:
        tmp = int(str1[max_length - 1]) + int(str2[max_length - 1]) + int(bit)
        if tmp == 3:
            res_list = '1' + res_list
            bit = 1
        elif tmp == 2:
            res_list = '0' + res_list
            bit = 1
        else:
            res_list = str(tmp) + res_list
            bit = 0
        max_length -= 1
    if bit == 1:
        res_list = str(bit) + res_list
    return res_list


if __name__ == '__main__':
    binary_str1 = "1111"
    binary_str2 = "1111"
    assert add_binary(binary_str1, binary_str2) == "11110"
