# --*-- coding:utf-8 --*--
"""
==input==   两个只相差一个小写字母的字符串
==******==
==output==  找出这个小写字母
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的空间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def find_the_difference(str1, str2):
    if str1 is None or str2 is None or abs(len(str2) - len(str1)) != 1:
        return None
    return method1(str1, str2)


# 异或的性质
# 1、异或具有交换律：a ^ b ^ c  <===>  c ^ b ^ a
# 2、两个相同的数异或，结果为0
# 3、0与任何数异或，结果还是这个数
def method1(str1, str2):
    min_length = min(len(str1), len(str2))
    res = ord(str2[-1]) if min_length == len(str1) else ord(str1[-1])
    for i in range(min_length):
        res ^= ord(str1[i]) ^ ord(str2[i])
    return chr(res)


if __name__ == '__main__':
    input_str1, input_str2 = "abfde", "dbqfea"
    assert find_the_difference(input_str1, input_str2) == "q"
