# -*- coding:utf-8 -*-
"""
==input==   给定一个只含有小写、大写、空格的字符串
==******==  单词之间用空格分隔（可能有多个空格），空格不算做单词
==output==  最后一个单词的长度，没有最后一个单词时返回0
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 注意边界值的处理
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def length_last_word(input_str):
    if input_str is None or len(input_str.strip()) == 0:
        return 0
    return method1(input_str.strip())


def method1(input_str):
    res = 0
    index = len(input_str) - 1
    while index >= 0 and input_str[index] != ' ':
        res += 1
        index = index - 1
    return res


if __name__ == '__main__':
    input_string = "a"
    assert length_last_word(input_string) == 1
