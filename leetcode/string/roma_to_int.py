# -*- coding:utf-8 -*-
"""
==input==   给定一个罗马数字的字符串
==******==  罗马数字与十进制整数的对应关系  'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000
==******==  注意：像 C 是可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900的
==output==  罗马数字字符串对应的十进制整数值
##########  在O（1）的空间复杂度和低于O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 字符串末位的处理
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def roma_to_int(roma_str):
    if roma_str is None or len(roma_str) == 0:
        return
    return method1(roma_str)


def method1(roma_str):
    value_mapping = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
    res = 0
    start = 0
    next_index = start + 1
    while start < len(roma_str) and next_index < len(roma_str):
        key, next_key = roma_str[start], roma_str[next_index]
        if value_mapping[key] >= value_mapping[next_key]:
            res = res + value_mapping[key]
            start = start + 1
            next_index = next_index + 1
        else:
            res = res + value_mapping[next_key] - value_mapping[key]
            start = start + 2
            next_index = next_index + 2
    if start < len(roma_str):
        res = res + value_mapping[roma_str[start]]
    return res


if __name__ == '__main__':
    roma_str = "MCMXCIV"
    assert roma_to_int(roma_str) == 1994
