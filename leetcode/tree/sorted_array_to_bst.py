# -*- coding:utf-8 -*-
from random import randint
"""
==input==   有序的整数数组
==******==
==output==  有序数组对应的排序二叉树（不唯一）
##########  在O（N）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 数组与树的相互转换，排序二叉树的性质，树的深度优先遍历 | 树的广度优先遍历
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def sorted_array_to_bst(nums):
    if nums is None or len(nums) == 0 or len(nums) == 1:
        return nums
    return method1(nums)


# 始终选择中间节点左边的元素作为根节点
def method1(nums):
    def helper(left, right):
        if left > right:
            return None
        p = (left + right) // 2
        root = TreeNode(nums[p])
        root.left = helper(left, p - 1)
        root.right = helper(p + 1, right)
        return root
    return helper(0, len(nums) - 1)


# 始终选择中间节点右边的节点作为根元素
def method2(nums):
    def helper(left, right):
        if left > right:
            return
        p = (left + right) // 2
        if (left + right) % 2:
            p += 1
        root = TreeNode(nums[p])
        root.left = helper(left, p - 1)
        root.right = helper(p + 1, right)
        return root
    return helper(0, len(nums) - 1)


# 始终选择中间节点两边的任意一个元素作为根节点
def method3(nums):
    def helper(left, right):
        if left > right:
            return
        p = (left + right) // 2
        if (left + right) % 2:
            p += randint(0, 1)
        root = TreeNode(nums[p])
        root.left = helper(left, p - 1)
        root.right = helper(p + 1, right)
        return root
    return helper(0, len(nums) - 1)


# 树的广度优先遍历
def tree_bfs_to_list(root):
    def tree_bfs(rt, res):
        if rt is None:
            return
        else:
            queue = list()
            queue.append(rt)
            while queue:
                cur = queue.pop(0)
                res.append(cur.val)
                if cur.left is not None:
                    queue.append(cur.left)
                if cur.right is not None:
                    queue.append(cur.right)
        return res
    return tree_bfs(root, list())


# 树的深度优先遍历 --- 前序遍历
def tree_dfs_pre_order_to_list(root):
    def tree_dfs_pre_order(rt, res):
        if rt is None:
            return
        res.append(rt.val)
        tree_dfs_pre_order(rt.left, res)
        tree_dfs_pre_order(rt.right, res)
        return res
    return tree_dfs_pre_order(root, list())


# 树的深度优先遍历 --- 中序遍历
def tree_dfs_mid_order_to_list(root):
    def tree_dfs_mid_order(rt, res):
        if rt is None:
            return
        tree_dfs_mid_order(rt.left, res)
        res.append(rt.val)
        tree_dfs_mid_order(rt.right, res)
        return res
    return tree_dfs_mid_order(root, list())


# 树的深度优先遍历 --- 后序遍历
def tree_dfs_post_order_to_list(root):
    def tree_dfs_post_order(rt, res):
        if rt is None:
            return
        tree_dfs_post_order(rt.left, res)
        tree_dfs_post_order(rt.right, res)
        res.append(rt.val)
        return res
    return tree_dfs_post_order(root, list())


if __name__ == '__main__':
    input_list = [-10, -3, 0, 5, 9]
    assert tree_dfs_mid_order_to_list(sorted_array_to_bst(input_list)) == [-10, -3, 0, 5, 9]

