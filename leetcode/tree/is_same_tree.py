# --*-- coding:utf-8 --*--
"""
==input==   两个广度优先遍历的二叉树对应的数组
==******==
==output==  这两棵树是否相等
##########  在O（N）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 数组与树的相互转换，非递归实现的理解
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


def is_same_tree(rt_a, rt_b):
    if not rt_a and not rt_b:
        return True
    if (rt_a and not rt_b) or (not rt_a and rt_b):
        return False
    return method2(rt_a, rt_b)


# 递归写法
def method1(rt_a, rt_b):
    if not rt_a and not rt_b:
        return True
    if rt_a and rt_b and rt_a.val == rt_b.val:
        return method1(rt_a.left, rt_b.left) and method1(rt_a.right, rt_b.right)
    else:
        return False


# 借助栈，非递归写法
def method2(rt_a, rt_b):
    stack_a, stack_b = [rt_a], [rt_b]
    while stack_a or stack_b:
        cur_a, cur_b = stack_a.pop(), stack_b.pop()
        if not cur_a and not cur_b:
            continue
        if cur_a and cur_b and cur_a.val == cur_b.val:
            stack_a.append(cur_a.left)
            stack_b.append(cur_b.left)
            stack_a.append(cur_a.right)
            stack_b.append(cur_b.right)
        else:
            return False
    return True


def list_bfs_to_binary_tree(nums, index):
    rt = None
    if not nums:
        return rt
    if index < len(nums):
        if not nums[index]:
            pass
        else:
            rt = TreeNode(nums[index])
            rt.left = list_bfs_to_binary_tree(nums, 2 * index + 1)
            rt.right = list_bfs_to_binary_tree(nums, 2 * index + 2)
    return rt


if __name__ == '__main__':
    input_list_a = [1, 2, None, 5, None, None, None, 8]
    input_list_b = [1, 2, None, 5, None, None, None, 8]
    root_a = list_bfs_to_binary_tree(input_list_a, 0)
    root_b = list_bfs_to_binary_tree(input_list_b, 0)
    assert is_same_tree(root_a, root_b) is True
