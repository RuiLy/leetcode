# --*-- coding:utf-8 --*--
"""
==input==   二叉树广度优先遍历对应的数组
==******==
==output==  二叉树所有左叶子节点的和
##########  在O（N）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 数组与树的相互转换，如何判断是左叶子节点
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


def sum_of_left_leaves(rt):
    if not rt or (not rt.left and not rt.right):
        return 0
    return method1(rt)


# 借助栈 + 深度优先搜索实现
def method1(rt):
    left_list = list()
    stack = [rt]
    while stack:
        cur = stack.pop()
        children = [cur.left, cur.right]
        if children[0] and not children[0].left and not children[0].right:
            left_list.append(children[0].val)
        for child in children:
            if child:
                stack.append(child)
    return sum(left_list)


def list_bfs_to_binary_tree(nums, index):
    rt = None
    if not nums:
        return rt
    if index < len(nums):
        if not nums[index]:
            pass
        else:
            rt = TreeNode(nums[index])
            rt.left = list_bfs_to_binary_tree(nums, 2 * index + 1)
            rt.right = list_bfs_to_binary_tree(nums, 2 * index + 2)
    return rt


if __name__ == '__main__':
    input_list = [3, None, 10]
    root = list_bfs_to_binary_tree(input_list, 0)
    assert sum_of_left_leaves(root) == 0
