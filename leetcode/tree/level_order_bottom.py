# --*-- coding:utf-8 --*--
import collections
"""
==input==   二叉树广度优先遍历对应的数组
==******==
==output==  从下往上按层次遍历的二维数组
##########  在O（N）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 数组与树的相互转换，层次遍历
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


def level_order_bottom(rt):
    if not rt:
        return []
    if not rt.left and not rt.right:
        return [[rt.val]]
    return method1(rt)


# 利用辅助队列实现
def method1(rt):
    dq = collections.deque()
    res = list()
    dq.append(rt)
    while dq:
        level = []
        for _ in range(len(dq)):
            cur = dq.popleft()
            level.append(cur.val)
            if cur.left:
                dq.append(cur.left)
            if cur.right:
                dq.append(cur.right)
        res.insert(0, level)
    return res


# 将广度优先遍历的数组，还原为二叉树
def list_bfs_to_binary_tree(nums, index):
    rt = None
    if not nums:
        return rt
    if index < len(nums):
        if nums[index] is None:
            pass
        else:
            rt = TreeNode(nums[index])
            rt.left = list_bfs_to_binary_tree(nums, 2 * index + 1)
            rt.right = list_bfs_to_binary_tree(nums, 2 * index + 2)
    return rt


if __name__ == '__main__':
    input_list = [3, 9, 20, None, None, 15, 7]
    root = list_bfs_to_binary_tree(input_list, 0)
    assert level_order_bottom(root) == [[15, 7], [9, 20], [3]]
