# -*- coding:utf-8 -*-
import collections
"""
==input==   二叉树广度优先遍历对应的数组
==******==
==output==  二叉树最大的深度
##########  在O（N）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 数组与树的相互转换，非递归求解方式
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def max_depth(rt):
    if not rt:
        return 0
    if not rt.left and not rt.right:
        return 1
    return method2(rt)
    # rs = Resolve(0)
    # return rs.method3(rt, 0)


# 递归求解方式
def method1(rt):
    return 0 if not rt else max(max_depth(rt.left), max_depth(rt.right)) + 1


# 非递归队列求解
def method2(rt):
    dq = collections.deque()
    dq.append(rt)
    depth = 0
    while dq:
        depth = depth + 1
        for _ in range(len(dq)):
            cur = dq.popleft()
            if cur.left:
                dq.append(cur.left)
            if cur.right:
                dq.append(cur.right)
    return depth


# 借助递归栈实现
class Resolve:
    def __init__(self, max_dep):
        self.max_dep = max_dep

    def method3(self, rt, level):
        if not rt:
            return
        if self.max_dep < level + 1:
            self.max_dep = level + 1
        self.method3(rt.left, level + 1)
        self.method3(rt.right, level + 1)
        return self.max_dep


# 将广度优先遍历的数组，转换为对应的二叉树
def list_bfs_to_binary_tree(nums, index):
    rt = None
    if not nums:
        return rt
    if index < len(nums):
        if nums[index] is None:
            pass
        else:
            rt = TreeNode(nums[index])
            rt.left = list_bfs_to_binary_tree(nums, 2 * index + 1)
            rt.right = list_bfs_to_binary_tree(nums, 2 * index + 2)
    return rt


if __name__ == '__main__':
    input_list = [3, 9, 20, None, None, 15, 7, None, None, None, None, None, None, None, 2]
    root = list_bfs_to_binary_tree(input_list, 0)
    assert max_depth(root) == 4
