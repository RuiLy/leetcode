# --*-- coding:utf-8 --*--
import collections
"""
==input==   二叉树广度优先遍历对应的数组
==******==
==output==  二叉树最小的深度
##########  在O（N）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 数组与树的相互转换，非递归求解方式
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


def min_depth(rt):
    if not rt:
        return 0
    if not rt.left and not rt.right:
        return 1
    return method3(rt)


# 转换一下思维
# 1、根节点只有右子树，树的最小深度就是右子树的深度
# 2、根节点只有左子树，树的最小深度就是左子树的深度
# 3、根节点只有左子树和右子树，树的最小深度就是左子树和右子树的最小深度
def method1(rt):
    if not rt:
        return 0
    if not rt.left and rt.right:
        return method1(rt.right) + 1
    if not rt.right and rt.left:
        return method1(rt.left) + 1
    return min(method1(rt.left), method1(rt.right)) + 1


# 借助栈深度优先搜索
def method2(rt):
    stack, min_dth = [(1, rt)], float("inf")
    while stack:
        depth, cur = stack.pop()
        children = [cur.left, cur.right]
        if not any(children):
            min_dth = min(depth, min_dth)
        for child in children:
            if child:
                stack.append((depth + 1, child))
    return int(min_dth)


# 借助队列广度优先搜索
def method3(rt):
    dq = collections.deque([(1, rt)])
    while dq:
        depth, cur = dq.popleft()
        children = [cur.left, cur.right]
        if not any(children):
            return depth
        for child in children:
            if child:
                dq.append((depth + 1, child))


def list_bfs_to_binary_tree(nums, index):
    rt = None
    if not nums:
        return rt
    if index < len(nums):
        if nums[index] is None:
            pass
        else:
            rt = TreeNode(nums[index])
            rt.left = list_bfs_to_binary_tree(nums, 2 * index + 1)
            rt.right = list_bfs_to_binary_tree(nums, 2 * index + 2)
    return rt


if __name__ == '__main__':
    input_list = [-9, -3, 2, None, 4, 4, 0, None, None, -6, None, -5]
    root = list_bfs_to_binary_tree(input_list, 0)
    assert min_depth(root) == 3
