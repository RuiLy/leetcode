# --*-- coding:utf-8 --*--
"""
==input==   LFU缓存算法实现
==******==
==output==
##########  在O（N）的空间复杂度和低于O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
这儿使用一种O（N）的方式完成，易于理解；难点 O（1）的时间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""
import sys


class Node:
    def __init__(self, key=None, val=None, times=sys.maxsize):
        self.key = key
        self.val = val
        self.times = times
        self.pre = None
        self.nex = None


class LFUCache:
    def __init__(self, capacity):
        self.capacity = capacity
        self.cache = {}
        self.head = Node(times=-sys.maxsize + 1)
        self.tail = Node(times=sys.maxsize)
        self.head.nex = self.tail
        self.tail.pre = self.head

    # 说明：双向链表的节点按照times从小到大进行连接，且times相同时，本次移动的节点尽量往后移动
    # 1、首先得到变动节点node的前一个节点和后一个节点
    # 2、如果变动节点的times 小于后一个节点的times，直接返回（意味着不需要重建双向链表）
    # 3、在双向链表中删除这个变动的节点node（改变2个指针）
    # 4、寻找应该把这个变动的节点node插入到双向链表的什么位置
    # 5、将变动节点node插入到双向链表中
    @staticmethod
    def _re_build(node):
        p_node, n_node = node.pre, node.nex
        if node.times < n_node.times:
            return
        p_node.nex, n_node.pre = n_node, p_node
        while node.times >= p_node.times:
            p_node = p_node.nex
        p_node = p_node.pre
        node.pre = p_node
        node.nex = p_node.nex
        p_node.nex = node
        p_node.nex.nex.pre = node

    # 1、如果key在缓存self.cache中，取出该节点，将times加1（使用次数增加），然后重建双向链表
    # 2、根据key查询缓存获取结果
    # a: 如果key在缓存中，返回val
    # b: 如果key不在缓存中，返回-1
    def get(self, key):
        if key in self.cache:
            node = self.cache[key]
            node.times += 1
            self._re_build(node)
        res = self.cache.get(key, None)
        return res.val if res else -1

    # 1、如果缓存self.cache的容量为0，直接返回（意味着不能存放元素）
    # 2、如果key在缓存self.cache中，取出key对应的节点node，其times加1（使用次数增加），更新node的val值为新值value，然后重建双向链表
    # 3、否则，先判断缓存self.cache是否满，如果满了，则在缓存中删除链表头结点下一个节点对应的key，同时删除双向链表头结点的下一个节点（改变两个指针）
    # 4、如果没有满，根据key value新建一个节点node，先把node放进缓存中self.cache中，再把node插入到双向链表的头部（改变4个指针），然后再重建双向链表
    def put(self, key, value):
        if self.capacity == 0:
            return
        if key in self.cache:
            node = self.cache[key]
            node.times += 1
            node.val = value
            self._re_build(node)
        else:
            if len(self.cache) == self.capacity:
                self.cache.pop(self.head.nex.key)
                self.head.nex = self.head.nex.nex
                self.head.nex.pre = self.head
            new = Node(key, value, 1)
            self.cache[key] = new
            self.head.nex.pre = new
            new.pre = self.head
            new.nex = self.head.nex
            self.head.nex = new
            self._re_build(new)


if __name__ == '__main__':
    lfu = LFUCache(2)
    lfu.put(1, 1)
    lfu.put(2, 2)
    assert lfu.get(1) == 1
    lfu.put(3, 3)
    assert lfu.get(2) == -1
    assert lfu.get(3) == 3
    lfu.put(4, 4)
    assert lfu.get(1) == -1
    assert lfu.get(3) == 3
    assert lfu.get(4) == 4
    assert list(lfu.cache.keys()) == [3, 4]
