# --*-- coding:utf-8 --*--
"""
==input==   LRU缓存算法实现
==******==
==output==
##########  在O（N）的空间复杂度和低于O（1）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的时间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


# 定义一个节点类，LRU（最近最少使用）
class Node:
    def __init__(self, key=None, val=None):
        self.key = key
        self.val = val
        self.pre = None
        self.nex = None


# 定义一个缓存类，里面需要一个容量 并 维护一个字典O（1）的获取和一个双向链表O（1）的更新/删除
class LRUCache:
    def __init__(self, capacity):
        self.capacity = capacity
        self.cache = {}
        self.head = Node()
        self.tail = Node()
        self.head.nex = self.tail
        self.tail.pre = self.head

    # 定义一个辅助函数，用来把key对应的node移动到双向链表的尾部（self.tail的前一个节点）
    # 1、从缓存中self.cache中得到该node
    # 2、从双向链表中删除该node（变动2个指针）
    # 3、把该node插入到双向链表的尾部（变动4个指针）
    def _move_end(self, key):
        # 根据key找出该节点
        node = self.cache[key]
        # 1、在原链表的基础上第一步先删除该节点
        node.nex.pre = node.pre
        node.pre.nex = node.nex
        # 2、将该节点插入到tail节点之前
        node.pre = self.tail.pre
        node.nex = self.tail
        self.tail.pre.nex = node
        self.tail.pre = node

    # 1、如果key在缓存中self.cache，把key对应的node移动到双向链表的尾部（表示最近经常使用）
    # 2、从缓存中self.cache取出node
    # a: 如果node为空，表示缓存中不存在对应的key 返回-1
    # b: 如果node不为空，返回对应的node.val
    def get(self, key):
        if key in self.cache:
            self._move_end(key)
        res = self.cache.get(key, None)
        return res.val if res else -1

    # 1、如果缓存self.cache的容量为0，直接返回（意味着不能存放元素）
    # 2、如果key在缓存中self.cache，更新node对应的val值，并把node移动到双向链表的尾部
    # 3、否则
    # a: 如果缓存时候已经满了，则删除双向链表头结点的下一个节点self.head.nex，同时在缓存中清楚这个节点对用的key
    # b: 新建一个节点，放入缓存，并把该节点插入到双向链表的尾部（表示最近经常使用）
    def put(self, key, value):
        if self.capacity == 0:
            return
        if key in self.cache:
            self.cache[key].val = value
            self._move_end(key)
        else:
            if len(self.cache) == self.capacity:
                # 删除该key在缓存中的记录
                self.cache.pop(self.head.nex.key)
                # 删除head之后的节点（最少访问的）
                self.head.nex = self.head.nex.nex
                self.head.nex.pre = self.head

            new = Node(key, value)
            # 更新缓存cache
            self.cache[key] = new
            # 新建一个node插入到tail节点之前
            new.pre = self.tail.pre
            new.nex = self.tail
            self.tail.pre.nex = new
            self.tail.pre = new


if __name__ == '__main__':
    lru = LRUCache(2)
    lru.put(2, 1)
    lru.put(1, 1)
    lru.put(2, 3)
    lru.put(4, 1)
    assert lru.get(1) == -1
    assert lru.get(2) == 3
    assert list(lru.cache.keys()) == [2, 4]
