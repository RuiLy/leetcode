# -*- coding:utf-8 -*-
"""
==input==   一个正整数
==******==
==output==  该正整数的平方根（向下取整）
##########  在O（1）的空间复杂度和O（logN）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 如何减少时间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def sqrt(num):
    if num < 0:
        return -1
    if num == 0 or num == 1:
        return num
    return method1(num)


# 牛顿法
def method1(num):
    cur = 1
    while True:
        pre = cur
        cur = (cur + num / cur) / 2
        if abs(cur - pre) <= 1e-9:
            return int(cur)


# 二分法
def method2(num):
    left = 1
    right = num // 2
    while left < right:
        middle = left + (right - left + 1) // 2
        if middle * middle > num:
            right = middle - 1
        else:
            left = middle
    return left


# 常规思路解法
def method3(num):
    k = 1
    while k <= num // 2:
        if k * k <= num < (k + 1) * (k + 1):
            return k
        k += 1
    return -1


if __name__ == '__main__':
    input_num = 12
    assert sqrt(input_num) == 3
