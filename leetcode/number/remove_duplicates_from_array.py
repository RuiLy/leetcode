# -*- coding: utf8 -*-
"""
==input==   给定一个按照升序排列的整数数组
==******==  要求从该有序整数数组中原地删除重复的整数
==output==  返回删除重复元素后，该数组的长度
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点在于要在原数组的基础上原地移动数据，不能开辟新的内存空间
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def remove_duplicates_from_array(nums_list):
    if nums_list is None or len(nums_list) == 0:
        return 0
    elif len(nums_list) == 1:
        return 1
    return method2(nums_list)


def method1(nums):
    for index in range(len(nums) - 1, 0, -1):
        if nums[index] == nums[index - 1]:
            del nums[index]
    return len(nums)


def method2(nums):
    tail = 1
    for head in range(1, len(nums) - 1):
        if nums[head] != nums[head - 1]:
            nums[tail] = nums[head]
            tail += 1
    return len(nums[:tail])


if __name__ == '__main__':
    input_list = [0, 1, 1, 2, 2, 2, 2, 2, 2, 3, 4, 4]
    remove_element = 2
    assert remove_duplicates_from_array(input_list) == 5
