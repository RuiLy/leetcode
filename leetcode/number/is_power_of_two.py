# --*-- coding:utf-8 --*--
"""
==input==    一个整数
==******==
==output==   是否是2的幂
##########  在O（1）的空间复杂度和O（1）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的时间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def is_power_of_two(num):
    if num <= 0:
        return False
    return method2(num)


# 布莱恩.克尼根算法
def method1(num):
    weight = 0
    while num:
        weight += 1
        num &= (num - 1)
    return weight == 1


# 移位法
def method2(num):
    weight = 0
    while num:
        if num & 1:
            weight += 1
        num >>= 1
    return weight == 1


# 库函数法
def method3(num):
    return bin(num)[2:].count('1') == 1


if __name__ == '__main__':
    input_num = 4
    assert is_power_of_two(input_num) is True
