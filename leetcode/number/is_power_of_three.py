# --*-- coding:utf-8 --*--
"""
==input==    一个整数
==******==
==output==  是否是3的幂
##########  在O（N）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def is_power_of_three(num):
    if num <= 0:
        return False
    return method2(num)


# 常规解法
def method2(num):
    while num % 3 == 0:
        num //= 3
    return num == 1


# 转换为3进制后，字符串只能包含一个 1
def method1(num):
    three_nary_str = convert(num, 3)
    return three_nary_str.count('1') == 1 and three_nary_str.count('0') == len(three_nary_str) - 1


# 进制转换辅助函数
def convert(num, cv):
    code = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C',
            'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O']
    res, res_str = list(), list()
    if 1 > cv or cv > 25:
        return None
    while True:
        merchant = num // cv
        remainder = num % cv
        res.append(remainder)
        if not merchant:
            break
        num = merchant
    res.reverse()
    for i in res:
        res_str.append(code[i])
    return "".join(res_str)


if __name__ == '__main__':
    input_num = 45
    assert is_power_of_three(input_num) is False
