# --*-- coding:utf-8 --*--
"""
==input==    1 2...N 组成的 N+1个整数数，其中一个重复的数
==******==
==output==  找出这个重复的数
##########  在O（1）的空间复杂度和低于O（N*N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的空间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def find_duplicate(nums):
    if nums is None or len(nums) <= 1:
        return -1
    return method2(nums)


# 二分法
def method1(nums):
    left, right = 0, len(nums)
    while left < right:
        cnt = 0
        mid = left + (right - left) // 2
        for num in nums:
            if num <= mid:
                cnt += 1
        if cnt > mid:
            right = mid
        else:
            left = mid + 1
    return left


# 快慢指针法,等价于链表中找出环
def method2(nums):
    slow, fast = nums[0], nums[0]
    # 循环结束，slow便是环的起点
    while True:
        slow = nums[slow]
        fast = nums[nums[fast]]
        if slow == fast:
            break
    before, after = nums[0], slow
    while before != after:
        before = nums[before]
        after = nums[after]
    return before


if __name__ == '__main__':
    input_nums = [5, 3, 2, 9, 1, 4, 6, 7, 8, 9]
    assert find_duplicate(input_nums) == 9
