# -*- coding: utf8 -*-
"""
==input==   给定一个整数数组，和一个待删除的目标值
==******==  要求从该整数数组中删除与目标值相等的所有元素，不能改变原数组
==output==  返回删除目标元素后，该数组剩余元素的个数
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点在于要在原数组的基础上原地移动数据，不能开辟新的内存空间
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def remove_element_from_array(nums_list, target):
    if nums_list is None or len(nums_list) == 0:
        return 0
    return method3(nums_list, target)


def method1(nums, val):
    while val in nums:
        nums.remove(val)
    return len(nums)


def method2(nums, val):
    length = len(nums) - 1
    for index in range(length, -1, -1):
        if val == nums[index]:
            nums.pop(index)
    return len(nums)


def method3(nums, val):
    counter = 0
    for index, item in enumerate(nums):
        if item != val:
            nums[counter] = nums[index]
            counter = counter + 1
    return len(nums[:counter])


if __name__ == '__main__':
    input_list = [0, 1, 2, 2, 3, 0, 4, 2]
    remove_element = 2
    assert remove_element_from_array(input_list, remove_element) == 5
