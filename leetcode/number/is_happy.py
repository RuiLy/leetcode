# --*-- coding:utf-8 --*--
"""
==input==   一个正整数
==******==  对于一个正整数，每一次将该数替换为它每个位置上的数字的平方和，然后重复这个过程直到这个数变为 1，
==******==  也可能是无限循环但始终变不到 1。如果可以变为1，那么这个数就是快乐数
==******==  eg: input 19
==******==  1^2 + 9^2 = 82
==******==  8^2 + 2^2 = 68
==******==  6^2 + 8^2 = 100
==******==  1^2 + 0^2 + 0^2 = 1 （满足条件）
==output==  True/ False 是否是快乐数
##########  在O（1）的空间复杂度和低于O（logN）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的空间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def is_happy(num):
    if num <= 0:
        return False
    if num == 1:
        return True
    return method2(num)


# 根据当前的数得到下一个数
def get_next(num):
    sqrt = 0
    while num:
        num, yu = divmod(num, 10)
        sqrt += yu ** 2
    return sqrt


# 如果不是快乐数，不断的得到下一个数，势必会遇到环
# 所以用set集合来存储，历史得到过的数，如果当前得到的数已经在set中，那么一定不是快乐数
# 否则判断最终得到的数是否是1
def method1(num):
    hash_sets = set()
    while num != 1:
        if num in hash_sets:
            return False
        hash_sets.add(num)
        num = get_next(num)
    return num == 1


# 采用快慢指针，判断单向链表中是否存在环的方式
def method2(num):
    low, fast = num, get_next(num)
    while fast != 1 and low != fast:
        low = get_next(low)
        fast = get_next(get_next(fast))
    return fast == 1


if __name__ == '__main__':
    input_num = 19
    assert is_happy(input_num) is True
