# --*-- coding:utf-8 --*--
"""
==input==    一个整数
==******==
==output==  是否是4的幂
##########  在O（N）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点O(1)的空间复杂度和O(1)的时间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def is_power_of_four(num):
    if num <= 0:
        return False
    return method1(num)


def method1(num):
    return num > 0 and num & (num - 1) == 0 and num % 3 == 1


def method2(num):
    return num > 0 and num & (num - 1) == 0 and num & 0xaaaaaaaa == 0


def method3(num):
    while not num % 4:
        num //= 4
    return num == 1


def method4(num):
    four_nary_str = covert(num, 4)
    return four_nary_str.count('1') == 1 and four_nary_str.count('0') == len(four_nary_str) - 1


# 辅助进制转换函数
def covert(num, nv):
    if nv <= 0 or nv > 16:
        return
    dict_code = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E']
    res, res_str = list(), list()
    while True:
        shang = num // 4
        yushu = num % 4
        res.append(yushu)
        if not shang:
            break
        num = shang
    res.reverse()
    for i in res:
        res_str.append(dict_code[i])
    return "".join(res_str)


if __name__ == '__main__':
    input_num = 64
    assert is_power_of_four(input_num) is True
