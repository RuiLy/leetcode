# -*- coding:utf8 -*-
"""
==input==   给定一个有序的整数数组
==******==  要求从该有序整数数组中原地删除重复元素（相同元素只保留一个）
==output==  返回去重后，数组元素的个数
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点在于要在原数组的基础上原地删除重复元素，不能开辟新的内存空间
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def remove_same_element_from_sort_array(nums):
    if nums is None or len(nums) == 0:
        return 0
    return method2(nums)


def method1(nums):
    temp = None
    for index in range(len(nums) - 1, -1, -1):
        if temp == nums[index]:
            nums.pop(index)
        else:
            temp = nums[index]
    return len(nums)


def method2(nums):
    counter = 0
    for item in nums[1:]:
        if nums[counter] != item:
            counter = counter + 1
            nums[counter] = item
    return len(nums[:counter + 1])


if __name__ == '__main__':
    input_nums = [0, 0, 0, 1, 1, 3, 5, 5, 6, 6]
    out_int = remove_same_element_from_sort_array(input_nums)
    assert out_int == 5
