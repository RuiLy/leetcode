# --*-- coding:utf-8 --*--
"""
==input==    一个无符号整数
==******==
==output==   对应二进制表示，1的总个数
##########  在O（1）的空间复杂度和O（1）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的时间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def hamming_weight(num):
    if num < 0:
        return -1
    if num == 0:
        return 0
    return method1(num)


# 布莱恩.克尼根算法
def method1(num):
    weight = 0
    while num:
        weight += 1
        num &= (num - 1)
    return weight


# 移位法
def method2(num):
    weight = 0
    while num:
        if num & 1:
            weight += 1
        num >>= 1
    return weight


# 库函数
def method3(num):
    return bin(num)[2:].count('1')


if __name__ == '__main__':
    input_num = 5
    assert hamming_weight(input_num) == 2
