# --*--coding:utf-8--*--
"""
==input==   数组中有一个数字出现的次数超过数组长度的一半
==******==
==output==  找出这个数字
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的空间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def majority_element(nums):
    if nums is None or len(nums) == 0:
        return
    return method1(nums)


# 投票法
# 可以求出出现次数最多的数
# 不一定非得某个数出现的次数超过数组的一半
def method1(nums):
    votes, target = 0, nums[0]
    for num in nums:
        if num == target:
            votes += 1
        else:
            votes -= 1
        if votes == 0:
            target = num
            votes += 1
    return target


# 统计法
def method2(nums):
    counter = dict()
    for num in nums:
        if num not in counter:
            counter[num] = 1
        else:
            counter[num] += 1
    for key in counter.keys():
        if counter[key] > len(nums) // 2:
            return key
    return 0


if __name__ == '__main__':
    input_nums = [1, 3, 9, 8, 2, 3, 3, 6, 2, 3, 3, 3, 3]
    assert majority_element(input_nums) == 3
