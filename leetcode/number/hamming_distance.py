# --coding:utf-8 --*--
"""
==input==    两个整数
==******==   例如：1 -> 0001  14 -> 1110 ,汉明距离为 4
==output==   汉明距离 （二进制表示时，对应位置不同的累加和）
##########  在O（1）的空间复杂度和O（1）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的时间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def hamming_distance(num_a, num_b):
    if num_a == num_b:
        return 0
    return method1(num_a, num_b)


# 布莱恩.克尼根算法
def method1(num_a, num_b):
    distance = 0
    orx = num_a ^ num_b
    while orx:
        distance += 1
        orx &= (orx - 1)
    return distance


# 移位操作 与 按位与 结合统计二进制1的个数
def method2(num_a, num_b):
    distance = 0
    orx = num_a ^ num_b
    while orx:
        if orx & 1:
            distance += 1
        orx >>= 1
    return distance


# 使用库函数
def method3(num_a, num_b):
    return bin(num_a ^ num_b).count('1')


if __name__ == '__main__':
    input_num_a, input_num_b = 1, 14  # 0001 ^ 1110 = 1111
    assert hamming_distance(input_num_a, input_num_b) == 4
