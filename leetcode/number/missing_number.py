# --*-- coding:utf-8 --*--
"""
==input==   0,1...N 缺失一个数的整数序列
==******==
==output==  缺失的那一个数
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的空间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def missing_number(nums):
    if nums is None or len(nums) == 0:
        return -1
    return method1(nums)


# 异或三大性质
# 1、异或运算满足交换律： a ^ b ^ c <===> c ^ a ^ b
# 2、相同的两个数异或，结果为0
# 3、0异或任何数，结果还是该数
# 6 ^ 4 ^ 0 ^ 6 ^ 1 ^ 3 ^ 2 ^ 0 ^ 3 ^ 1 ^ 4 ^ 5 ^ 5 == 2
def method1(nums):
    res = len(nums)
    for i in range(len(nums)):
        res ^= nums[i]
        res ^= i
    return res


# 公式法，求和可能越界
def method2(nums):
    return (0 + len(nums)) * (len(nums) + 1) / 2 - sum(nums)


if __name__ == '__main__':
    input_nums = [4, 6, 3, 0, 1, 5]
    assert missing_number(input_nums) == 2
