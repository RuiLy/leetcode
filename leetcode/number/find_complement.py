# --*-- coding:utf-8 --*--
"""
==input==   输入一个正整数
==******==  例如：5 -> 101  补数为 2 -> 10
==output==  补数
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的空间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def find_complement(num):
    if num is None:
        return
    return method2(num)


# 采用异或
# 101 ^ 111 == 010  => 2
def method2(num):
    return num ^ int(len(bin(num)[2:]) * '1', 2)


# 直接解法
def method1(num):
    binary_str = bin(num)[2:]
    res_binary = ['0', 'b']
    for bi in binary_str:
        if bi == '0':
            res_binary.append('1')
        else:
            res_binary.append('0')
    return int("".join(res_binary), 2)


if __name__ == '__main__':
    input_num = 2
    assert find_complement(input_num) == 1
