# --*-- coding:utf-8 --*--
"""
==input==   非负整数 num
==******==  给定一个非负整数 num，反复将各个位上的数字相加，直到结果为一位数
==output==  这个数的数根
##########  在O（1）的空间复杂度和低于O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的时间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def add_digits(num):
    if num < 10:
        return num
    return method1(num)


# 数学法
def method1(num):
    return (num - 1) % 9 + 1


# 递归法
def method2(num):
    if num < 10:
        return num
    number = 0
    while num:
        num, mod = divmod(num, 10)
        number += mod
    return method2(number)


# 循环法
def method3(num):
    while num >= 10:
        number = 0
        while num:
            num, mod = divmod(num, 10)
            number += mod
        num = number
    return num


if __name__ == '__main__':
    input_num = 1234567
    assert add_digits(input_num) == 1

