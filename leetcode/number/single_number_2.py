# --*-- coding:utf-8 --*--
"""
==input==   只有一个不重复元素和只重复三次的元素一起组成的整形数组
==******==
==output==  该不重复元素
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的空间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def single_number_2(nums):
    if nums is None or (len(nums) <= 3 and len(nums) != 1):
        return
    if len(nums) == 1:
        return nums[0]
    return method1(nums)


def method1(nums):
    once, twice = 0, 0
    for num in nums:
        once = ~ twice & (once ^ num)
        twice = ~ once & (twice ^ num)
    return once


if __name__ == '__main__':
    input_nums = [2, 2, 2, 3, 5, 3, 3]
    assert single_number_2(input_nums) == 5
