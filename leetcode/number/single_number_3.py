# --*-- coding:utf-8 --*--
"""
==input==   只有两个不重复元素和只重复两次次的元素一起组成的整形数组
==******==
==output==  这两个不重复元素组成的列表
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的空间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def single_number_3(nums):
    if nums is None or len(nums) <= 1:
        return
    if len(nums) == 2 and nums[0] != nums[1]:
        return nums
    return method1(nums)


# 将该数组拆分为分别只含有一个不同数的两个数组，
# 重复的数字肯定会到同一个数组中去
# 每个数组运用异或运算，求出各自不重复的那个元素
def method1(nums):
    mask, one_dif, two_dif = 0, 0, 0
    for num in nums:
        mask ^= num
    k = mask & (- mask)
    for num in nums:
        if num & k:
            one_dif ^= num
        else:
            two_dif ^= num
    return sorted([one_dif, two_dif])


if __name__ == '__main__':
    input_nums = [1, 1, 5, 5, 3, 9, 6, 9]
    assert single_number_3(input_nums) == [3, 6]
