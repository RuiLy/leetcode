# --*-- coding:utf-8 --*--
"""
==input==   只有一个不重复元素和只重复两次的元素一起组成的整形数组
==******==
==output==  该不重复元素
##########  在O（1）的空间复杂度和O（N）的时间复杂度下完成
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（1）的空间复杂度
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def single_number(nums):
    if nums is None or len(nums) == 0:
        return
    return method1(nums)


# 异或三大性质
# 1、异或具有交换律：a^b^c <===> a^c^b
# 2、两个相同的数异或，结果为0
# 3、0与任何数异或，结果还是该数
def method1(nums):
    res = 0
    for num in nums:
        res ^= num
    return res


if __name__ == '__main__':
    input_nums = [4, 2, 1, 1, 2, 3, 3]
    assert single_number(input_nums) == 4
