# -*- coding:utf-8 -*-
"""
==input==   无序的整数列表 nums
==******==
==output==  第一个不在列表中的正整数
##########  在O（1）的空间复杂度和低于O（N）的时间复杂度下完成
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
难点 O（N）的时间复杂度
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
"""


def first_miss_number(nums):
    if nums is None or len(nums) < 1:
        return -1
    return method1(nums)


# [3,5,1,2,-3,7,14,8]
def method1(nums):
    length = len(nums)  # 8
    nums.insert(0, None)  # [None,3,5,1,2,-3,7,14,8]
    i = 1
    while i <= length:
        if nums[i] > length or nums[i] < i:
            nums[i] = nums[length]
            length -= 1
        elif nums[i] > i:
            tmp = nums[i]
            nums[i] = nums[tmp]
            nums[tmp] = tmp
        else:
            i += 1
    return i


def method2(nums):
    length = len(nums)
    i = 0
    while i < length:
        if nums[i] > length or nums[i] < i - 1:
            nums[i] = nums[length - 1]
            length -= 1
        elif nums[i] > i + 1:
            tmp = nums[nums[i] - 1]
            nums[nums[i] - 1] = nums[i]
            nums[i] = tmp
        else:
            i += 1
    return i + 1


# [3,5,1,2,-3,7,14,8]
def method3(nums):
    length = len(nums)  # 8
    result = 1
    i = 0
    while i < length:
        if nums[i] != result:
            i += 1
        else:
            result += 1
            i = 0
    return result


if __name__ == '__main__':
    input_list = list(map(lambda x: int(x), input().split(",")))
    print(input_list)
    assert first_miss_number(input_list) == 4
